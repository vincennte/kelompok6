@extends('admin.master')
@section('subtitle')
Data Tabel
@endsection
@section('content')
<a href="admin/create" class="btn btn-primary">Tambah Makanan</a>
<a href="/pdf/cetak_pdf" class="btn btn-warning">Download PDF</a>
<table class="table">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama Makanan</th>
        <th scope="col">Deskripsi</th>
        <th scope="col">Harga</th>
        <th scope="col">Kategori</th>
        <th scope="col">Detail</th>
        <th scope="col">Aksi</th>
        
      </tr>
    </thead>
    <tbody>
        @foreach ($adminModel as $admin=>$a)
        <tr>
            <th scope="row">{{$admin+1}}</th>
            <td>{{$a->name}}</td>
            <td>{{Str::limit($a->description,50)}}</td>
            <td>{{$a->price}}</td>
            <td>{{$a->kategori}}</td>
            <td><a href="admin/{{$a->id}}" class="btn btn-info">Detail Data</a></td>
            <td>
                <form method="post" action="/admin/{{$a->id}}">
                    @csrf
                    @method('delete')
                <a href="admin/{{$a->id}}/edit" class="btn btn-warning">Edit</a>
                <button type="submit" class="btn btn-danger">Hapus</button>
                </form>
            </td>
          </tr>    
        @endforeach
    </tbody>
  </table>
  @section('javascript')
  <script src="{{asset('/template/adminLTE/dist/js/swal.min.js')}}"></script>
  <script>
    Swal.fire({
        title: "Admin Page",
        text: "Ini adalah admin page untuk menambahkan daftar menu",
        icon: "warning",
        confirmButtonText: "Oke!",
    });
</script>
  @endsection
@endsection