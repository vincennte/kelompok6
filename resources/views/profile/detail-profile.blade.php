@extends('admin.master')
@section('title')
Detail Profile
@endsection
@section('subtitle')
Detail Profile
@endsection
@section('content')
<div class="row">
  <div class="col-6">
    <h3> Name : {{$profileModel->name}}</h3>
    <h5> Phone:  {{$profileModel->phone}}</h5>
    {{--<h5> <b> Kategori :  {{$kategoriModel->name}}</b> </h5>--}}
    <h5> Address : {{$profileModel->address}}</h5>
  </div>
  <div class="col-6">
    <center><img src="{{ asset("/uploads/$profileModel->thumbnail") }}" width="400px"; /> </center>
  </div>
<div class="row" style="margin:10px;">
  <a href="/profile" class="btn btn-primary">Kembali</a>
</div>
  
@endsection