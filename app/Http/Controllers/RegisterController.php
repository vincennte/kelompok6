<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use App\Models\Profile;
use Illuminate\Foundation\Auth\Register;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{


    use RegisterUsers;

    protected $redirecteTo = RouteServiceProvider::HOME;
    public function bikinakun()
    {
        return view('layouts.register');
        
    }
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'username' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email' ,'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'phone' => ['required'],
            'alamat' => ['required'],
            'g-recaptcha-response' => 'recaptcha',
        ]);

        $validatedData['password'] = Hash::make($validatedData['password']);
 
        User::create($validatedData);

        Profile::create([
            'name' => $data['name'],
            'phone' => $data['phone'],
            'address' => $data['address'],
            'user_id' => $user->id
        ]);
        return redirect('/login')->with('success', 'Registration Succesfull! Please Login');
    }
}

