
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Review Makanan</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Free HTML5 Template by FREEHTML5.CO" />
	<meta name="keywords" content="free html5, free template, free bootstrap, html5, css3, mobile first, responsive" />
	<meta name="author" content="FREEHTML5.CO" />

  <!-- 
	//////////////////////////////////////////////////////

	FREE HTML5 TEMPLATE 
	DESIGNED & DEVELOPED by FREEHTML5.CO
		
	Website: 		http://freehtml5.co/
	Email: 			info@freehtml5.co
	Twitter: 		http://twitter.com/fh5co
	Facebook: 		https://www.facebook.com/fh5co

	//////////////////////////////////////////////////////
	 -->

  	<!-- Facebook and Twitter integration -->
	<meta property="og:title" content=""/>
	<meta property="og:image" content=""/>
	<meta property="og:url" content=""/>
	<meta property="og:site_name" content=""/>
	<meta property="og:description" content=""/>
	<meta name="twitter:title" content="" />
	<meta name="twitter:image" content="" />
	<meta name="twitter:url" content="" />
	<meta name="twitter:card" content="" />

	<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
	<link rel="shortcut icon" href="favicon.ico">

	<link href='https://fonts.googleapis.com/css?family=Playfair+Display:400,700,400italic,700italic|Merriweather:300,400italic,300italic,400,700italic' rel='stylesheet' type='text/css'>
	
	<!-- Animate.css -->
	<link rel="stylesheet" href="{{asset('/template/css/animate.css')}}">
	<!-- Icomoon Icon Fonts-->
	<link rel="stylesheet" href="{{asset('/template/css/icomoon.css')}}">
	<!-- Simple Line Icons -->
	<link rel="stylesheet" href="{{asset('/template/css/simple-line-icons.css')}}">
	<!-- Datetimepicker -->
	<link rel="stylesheet" href="{{asset('/template/css/bootstrap-datetimepicker.min.css')}}">
	<!-- Flexslider -->
	<link rel="stylesheet" href="{{asset('/template/css/flexslider.css')}}">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="{{asset('/template/css/bootstrap.css')}}">

	<link rel="stylesheet" href="{{asset('/template/css/style.css')}}">


	<!-- Modernizr JS -->
	<script src="{{asset('/template/js/modernizr-2.6.2.min.js')}}"></script>
	<!-- FOR IE9 below -->
	<!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
	<![endif]-->
	</head>
	<body>
		<div class="js-sticky">
			<div class="fh5co-main-nav">
				<div class="container-fluid">
					<div class="fh5co-logo">
						<a href="index.html">foodee</a>
					</div>
				
					<div class="fh5co-menu-2">
						@guest
						<a href="/register">Register</a>
						<a href="/login">Login</a>
						@endguest
						@auth
	
						<a href="{{ route('logout') }}"
						   onclick="event.preventDefault();
										 document.getElementById('logout-form').submit();">
							Logout
						</a>
						<form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
							@csrf
						</form>
					</div>
						@endauth
					
					
				</div>
				
				
			</div>
		</div>
		<div id="fh5co-menus" data-section="menu">
			<div class="container">
				<div class="row text-center fh5co-heading row-padded">
					<div class="col-md-8 col-md-offset-2">
						<h2 class="heading to-animate">Review Makanan</h2>
						<p class="sub-heading to-animate">Bantu pelayanan dan kualitas kami melalui feedback, Pilih Menu yang akan di review!</p>
					</div>
				</div>
				<div class="row row-padded">
					<div class="col-md-6">
						<div class="fh5co-food-menu to-animate-2">
							<h2 class="fh5co-drinks">Drinks</h2>
							<ul>
								@foreach ($adminModel as $admin)
								<li> <a href="user/{{$admin->id}}">
									<div class="fh5co-food-desc">
										<figure>
											<img src="{{ asset("/uploads/$admin->thumbnail") }}" class="img-responsive" alt="Free HTML5 Templates by FREEHTML5.co">
										</figure>
										<div>
											<h3>{{$admin->name}}</h3>
											<p>{{$admin->description}}</p>
											<h3 hidden>{{$total = DB::table('review')->
												join('food','food.id','=','review.food_id')->
												where('review.food_id',$admin->id)->
												count('review.food_id');}}</h3>
											<h3 hidden>{{$bintang = DB::table('review')->
												join('food','food.id','=','review.food_id')->
												where('review.food_id',$admin->id)->
												sum('review.rating');}}</h3>
											@if ($total===0)
												<h5>Jadilah Pertama yang review</h5>
											@else
												<h3>Rating : {{number_format($tampil=$bintang/$total,1)}} /5</h3>
											@endif
										</div>
									</div>
									<div class="fh5co-food-pricing">
										Rp.{{$admin->price}}
									</div>
								</a>
								</li>
								@endforeach
							</ul>
						</div>
					</div>
					<div class="col-md-6">
						<div class="fh5co-food-menu to-animate-2">
							<h2 class="fh5co-dishes">Steak</h2>
							@foreach ($adminModel2 as $admin2)
							<ul>
								<li> <a href="user/{{$admin2->id}}">
									<div class="fh5co-food-desc">
										<figure>
											<img src="{{ asset("/uploads/$admin2->thumbnail") }}" class="img-responsive" alt="Free HTML5 Templates by FREEHTML5.co">
										</figure>
										<div>
											<h3>{{$admin2->name}}</h3>
											<p>{{$admin2->description}}</p>
											<h3 hidden>{{$total = DB::table('review')->
												join('food','food.id','=','review.food_id')->
												where('review.food_id',$admin2->id)->
												count('review.food_id');}}</h3>
											<h3 hidden>{{$bintang = DB::table('review')->
												join('food','food.id','=','review.food_id')->
												where('review.food_id',$admin2->id)->
												sum('review.rating');}}</h3>
											@if ($total===0)
												<h5>Jadilah Pertama yang review</h5>
											@else
											<h3>Rating : {{number_format($tampil=$bintang/$total,1)}} /5</h3>
											@endif
										</div>
									</div>
									<div class="fh5co-food-pricing">
										Rp.{{$admin2->price}}
									</div>
								</a>
								</li>
							</ul>
							@endforeach
						</div>
					</div>
				</div>
			</div>
		</div>

		
	</body>
	<!-- jQuery -->
	<script src="{{asset('/template/js/jquery.min.js')}}"></script>
	<!-- jQuery Easing -->
	<script src="{{asset('/template/js/jquery.easing.1.3.js')}}"></script>
	<!-- Bootstrap -->
	<script src="{{asset('/template/js/bootstrap.min.js')}}"></script>
	<!-- Bootstrap DateTimePicker -->
	<script src="{{asset('/template/js/moment.js')}}"></script>
	<script src="{{asset('/template/js/bootstrap-datetimepicker.min.js')}}"></script>
	<!-- Waypoints -->
	<script src="{{asset('/template/js/jquery.waypoints.min.js')}}"></script>
	<!-- Stellar Parallax -->
	<script src="{{asset('/template/js/jquery.stellar.min.js')}}"></script>

	<!-- Flexslider -->
	<script src="{{('/template/js/jquery.flexslider-min.js')}}"></script>
	<script>
		$(function () {
	       $('#date').datetimepicker();
	   });
	</script>
	<!-- Main JS -->
	<script src="{{asset('/template/js/main.js')}}"></script>

</html>

