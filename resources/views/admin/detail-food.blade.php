@extends('admin.master')
@section('title')
Detail Makanan
@endsection
@section('subtitle')
Detail Makanan
@endsection
@section('content')
<div class="row">
  <div class="col-6">
    <h3> Nama Makanan : {{$adminModel->name}}</h3>
    <h5> Harga : Rp. {{$adminModel->price}}</h5>
    {{--<h5> <b> Kategori :  {{$kategoriModel->name}}</b> </h5>--}}
    <h5> Deskripsi : {{$adminModel->description}}</h5>
  </div>
  <div class="col-6">
    <center><img src="{{ asset("/uploads/$adminModel->thumbnail") }}" width="400px"; /> </center>
  </div>
<div class="row" style="margin:10px;">
  <a href="/admin" class="btn btn-primary">Kembali</a>
</div>
  
@endsection