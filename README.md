# Final Project
## Kelompok 6
## Anggota Kelompok

- Mohammad Sebtian Ghibthah (@hypersomnya)
- Vincencius Maxwell (@vmxwell)
- Santoni (@ScytheLike)

## Tema Project
Aplikasi penilaian Review makanan pada Restoran Foodee

## ERD
<img src="public/ERD.png">


## Link Video 
- Link Demo Aplikasi : https://drive.google.com/drive/folders/1cCKdQPR7k76stngaXZmb7dY3TtZGcYIT?usp=sharing
- Link Depoy : https://kelompok6sanber.herokuapp.com/