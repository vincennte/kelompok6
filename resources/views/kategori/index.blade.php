@extends('admin.master')
@section('title')
Kategori
@endsection
@section('subtitle')
Kategori
@endsection
@section('content')
<a href="/kategori/create" class="btn btn-success">Tambah Kategori</a>
<table class="table">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama Kategori</th>
        <th scope="col">Aksi</th>
        
      </tr>
    </thead>
    <tbody>
        @foreach ($kategoriModel as $admin=>$a)
        <tr>
            <th scope="row">{{$admin+1}}</th>
            <td>{{$a->name}}</td>
            <td>
                <form method="post" action="/kategori/{{$a->id}}">
                    @csrf
                    @method('delete')
                <a href="kategori/{{$a->id}}/edit" class="btn btn-warning">Edit</a>
                <button type="submit" class="btn btn-danger">Hapus</a>
                </form>
            </td>
          </tr>    
        @endforeach
    </tbody>
  </table>
  
@endsection