@extends('user.template')
@section('title')
    Review Makanan
@endsection
@section('content')
<center>
<div class="row" style="margin:20px;">
    <div class="col-6">
      <h3> <b>{{$cek->name}}</b></h3>
      <h5> Harga : Rp. {{$cek->price}}</h5>
      <img src="{{ asset("/uploads/$cek->thumbnail") }}" width="400px"; />
      {{--<h5> <b> Kategori :  {{$kategoriModel->name}}</b> </h5>--}}
      <h5> Deskripsi : {{$cek->description}}</h5>
    </div>
    <div class="col-4">
      <div>
        <label for="rating-inline">Rating:</label>
        <b-form-rating id="rating-inline" inline value="4"></b-form-rating>
      </div>
      <form action="/user" method="POST">
        @csrf
        <input type="hidden" name="food_id" value="{{$cek->id}}">
        <input type="hidden" name="user_id" value="{{$userId = Auth::id();}}">    

          <div class="stars" >
            <input class="star star-5" id="star-5" type="radio" name="rating" value="5"/>
            <label class="star star-5" for="star-5"></label>
            <input class="star star-4" id="star-4" type="radio" name="rating" value="4"/>
            <label class="star star-4" for="star-4"></label>
            <input class="star star-3" id="star-3" type="radio" name="rating" value="3"/>
            <label class="star star-3" for="star-3"></label>
            <input class="star star-2" id="star-2" type="radio" name="rating" value="2"/>
            <label class="star star-2" for="star-2"></label>
            <input class="star star-1" id="star-1" type="radio" name="rating" value="1"/>
            <label class="star star-1" for="star-1"></label>
          </div>
          <textarea class="form-control" rows="3" style="width: 450px;" name="comment"> </textarea>
          <div class="row" style="margin:10px;">
              <button type="submit" class="btn btn-success ">Simpan</button>
              <a href="/user" class="btn btn-danger">Batal</a>
          </div>

      </form>
    </div>
</div>
<label for="rating-inline">Apa Kata Mereka ?</label>
<p hidden>
{{$nama = DB::table('users')->
join('review','review.user_id','=','users.id')->
join('food','food.id','=','review.food_id')->
where('review.food_id',$cek->id)->
select('users.username AS username','review.comment AS comment')->
get();
}}
</p>
<div style="width: 600px; height: 200px; overflow: scroll;">
  <ul class="list-group">
    @foreach ($nama as $l)
    <li class="list-group-item">
      <b><h4>{{$l->username}}</h4></b>
      <p> {{$l->comment}} </p>
    </li>
    @endforeach
    
  </ul>
</div>
</center>
@endsection