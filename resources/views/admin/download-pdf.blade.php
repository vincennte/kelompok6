<!DOCTYPE html>
<html>
<head>
	<title>Daftar Menu</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<style type="text/css">
		table tr td,
		table tr th{
			font-size: 9pt;
		}
	</style>
	<center>
		<h5>Daftar Menu</h5>
	</center>
 
	<table class="table">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">Nama Makanan</th>
            <th scope="col">Deskripsi</th>
            <th scope="col">Harga</th>
            <th scope="col">Kategori</th>
          </tr>
        </thead>
        <tbody>
            @foreach ($adminModel as $admin=>$a)
            <tr>
                <th scope="row">{{$admin+1}}</th>
                <td>{{$a->name}}</td>
                <td>{{Str::limit($a->description,50)}}</td>
                <td>{{$a->price}}</td>
                <td>{{$a->kategori}}</td>
              </tr>    
            @endforeach
        </tbody>
    </table>
 
</body>
</html>