@extends('admin.master')
@section('title')
Edit Makanan
@endsection
@section('subtitle')
Edit Makanan
@endsection
@section('content')
<form action="/admin/{{$adminModel->id}}" method="POST" enctype="multipart/form-data">
    @method('put')
    @csrf
    <div class="form-row">
      <div class="form-group col-md-8">
        <label>Nama Makanan</label>
        <input type="text" class="form-control"name="name" value="{{$adminModel->name}}">
      </div>
      <div class="form-group col-md-4">
        <label>Harga</label>
        <input type="number" class="form-control" name="price" value="{{$adminModel->price}}">
      </div>
    </div>
    <div class="form-group">
      <label for="">Deskripsi</label>
      <textarea class="form-control" name="description">{{$adminModel->description}} </textarea>
    </div>
    <div class="form-group">
        <label>Kategori Makanan</label>
        <select name="menu_id" class="form-control">
          <option selected>Pilih Kategori</option>
            @foreach ($kategoriModel as $kate)
            <option value="{{$kate->id}}">{{$kate->name}}</option>
            @endforeach
          
        </select>
      </div>
    <div class="form-group">
        <label>Upload Foto</label>
        <input type="file" class="form-control-file" name="thumbnail">
    </div>
    <button type="submit" class="btn btn-primary">Simpan</button>
    <a href="/admin" class="btn btn-danger">Batal</a>
</form>
@endsection