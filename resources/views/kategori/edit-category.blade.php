@extends('admin.master')
@section('title')
Tambah Kategori
@endsection
@section('subtitle')
Tambah Kategori
@endsection
@section('content')
<form action="/kategori/{{$kategori->id}}" method="POST">
    @method('put')
    @csrf 
    <div class="form-row">
      <div class="form-group col-md-8">
        <label>Nama Kategori</label>
        <input type="text" class="form-control" name="name" value="{{$kategori->name}}">
        <br>
        <button type="submit" class="btn btn-primary">Simpan</button>
        <a href="/kategori" class="btn btn-danger">Batal</a>
    </div>
</form>
@endsection