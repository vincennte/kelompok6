<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\IndexController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\profileController;
use App\Http\Controllers\adminController;
use App\Http\Controllers\kategoriController;
use App\Http\Controllers\userController;
use App\Http\Controllers\pdfs;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });


// ROUTE HOME / FRONTPAGE
Route::get('/',[IndexController::class, 'home']);
// Route::get('/register',[RegisterController::class, 'bikinakun']);
// Route::get('/login',[ProfileController::class, 'masuk']);
// Route::get('/profile',[ProfileController::class, 'editprofile']);
// Auth::routes();


Auth::routes();


//ROUTE UNTUK CRUD TABEL FOOD
Route::resource('admin', adminController::class);

//ROUTE UNTUK CRUD TABEL MENU
Route::resource('kategori', kategoriController::class);
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');



//ROUTE UNTUK CRUD TABEL PROFILE
Route::resource('profile', profileController::class);


//ROUTE UNTUK TAMPILAN USER
Route::get('/user', [userController::class, 'index'])->name('user');
Route::get('user/{id}',[userController::class,'review']);
Route::post('/user', [userController::class, 'rating']);

//PDF
Route::get('/pdf/cetak_pdf',[pdfs::class,'cetak_pdf']);