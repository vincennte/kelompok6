
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Foodee &mdash; 100% Free Fully Responsive HTML5 Template by FREEHTML5.co</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Free HTML5 Template by FREEHTML5.CO" />
	<meta name="keywords" content="free html5, free template, free bootstrap, html5, css3, mobile first, responsive" />
	<meta name="author" content="FREEHTML5.CO" />

  <!-- 
	//////////////////////////////////////////////////////

	FREE HTML5 TEMPLATE 
	DESIGNED & DEVELOPED by FREEHTML5.CO
		
	Website: 		http://freehtml5.co/
	Email: 			info@freehtml5.co
	Twitter: 		http://twitter.com/fh5co
	Facebook: 		https://www.facebook.com/fh5co

	//////////////////////////////////////////////////////
	 -->

  	<!-- Facebook and Twitter integration -->
	<meta property="og:title" content=""/>
	<meta property="og:image" content=""/>
	<meta property="og:url" content=""/>
	<meta property="og:site_name" content=""/>
	<meta property="og:description" content=""/>
	<meta name="twitter:title" content="" />
	<meta name="twitter:image" content="" />
	<meta name="twitter:url" content="" />
	<meta name="twitter:card" content="" />

	<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
	<link rel="shortcut icon" href="favicon.ico">

	<link href='https://fonts.googleapis.com/css?family=Playfair+Display:400,700,400italic,700italic|Merriweather:300,400italic,300italic,400,700italic' rel='stylesheet' type='text/css'>
	
	<!-- Animate.css -->
	<link rel="stylesheet" href="{{asset('/template/css/animate.css')}}">
	<!-- Icomoon Icon Fonts-->
	<link rel="stylesheet" href="{{asset('/template/css/icomoon.css')}}">
	<!-- Simple Line Icons -->
	<link rel="stylesheet" href="{{asset('/template/css/simple-line-icons.css')}}">
	<!-- Datetimepicker -->
	<link rel="stylesheet" href="{{asset('/template/css/bootstrap-datetimepicker.min.css')}}">
	<!-- Flexslider -->
	<link rel="stylesheet" href="{{asset('/template/css/flexslider.css')}}">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="{{asset('/template/css/bootstrap.css')}}">

	<link rel="stylesheet" href="{{asset('/template/css/style.css')}}">


	<!-- Modernizr JS -->
	<script src="{{asset('/template/js/modernizr-2.6.2.min.js')}}"></script>
	<!-- FOR IE9 below -->
	<!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
	<![endif]-->
	</head>
	<body>
		<div class="">
			<div class="fh5co-main-nav">
				<div class="container-fluid">
					<div class="fh5co-logo">
						<a href="index.html">foodee</a>
					</div>
				
					<div class="fh5co-menu-2">
						<a href="#" class="">Restoran Terbaik Se Asia Tenggara!</a>
						@auth
	
						<a href="{{ route('logout') }}"
						   onclick="event.preventDefault();
										 document.getElementById('logout-form').submit();">
							Logout
						</a>
						<form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
							@csrf
						</form>
					</div>
						@endauth
					
					
				</div>
				
				
			</div>
		</div>

		<div id="fh5co-about" data-section="about" style="height:490px">
			{{-- <div class="fh5co-2col fh5co-bg to-animate-2" style="background-image: url(images/res_img_1.jpg)"></div> --}}
			<div class="fh5co-2col fh5co-text" style="text-align: justify;">
				<h2 class="heading to-animate"></h2>
				<h2 class="heading to-animate">Restoran Foodee</h2>
				<p class="to-animate"><span class="firstcharacter">P</span>enilaian anda
				sangat mempengaruhi kami dalam pengembangan kualitas pelayanan kami,
				maka dari itu kami sangat senang jika anda memberikan review dari kami.
				<b>klik tombol Nilai Sekarang! untuk menilai kami</b> Silahkan Login,
				atau registrasi anggota baru!</p>
				<p class="text-center to-animate"><a href="/login" class="btn btn-primary btn-outline">Nilai Sekarang!</a></p>
			</div>
			<div class="fh5co-2col fh5co-bg to-animate-2" style="background-image: url(template/images/bg.jpg);"></div>
		</div>
	<!-- jQuery -->
	<script src="{{asset('/template/js/jquery.min.js')}}"></script>
	<!-- jQuery Easing -->
	<script src="{{asset('/template/js/jquery.easing.1.3.js')}}"></script>
	<!-- Bootstrap -->
	<script src="{{asset('/template/js/bootstrap.min.js')}}"></script>
	<!-- Bootstrap DateTimePicker -->
	<script src="{{asset('/template/js/moment.js')}}"></script>
	<script src="{{asset('/template/js/bootstrap-datetimepicker.min.js')}}"></script>
	<!-- Waypoints -->
	<script src="{{asset('/template/js/jquery.waypoints.min.js')}}"></script>
	<!-- Stellar Parallax -->
	<script src="{{asset('/template/js/jquery.stellar.min.js')}}"></script>

	<!-- Flexslider -->
	<script src="{{('/template/js/jquery.flexslider-min.js')}}"></script>
	<script>
		$(function () {
	       $('#date').datetimepicker();
	   });
	</script>
	<!-- Main JS -->
	<script src="{{asset('/template/js/main.js')}}"></script>

	</body>
</html>

