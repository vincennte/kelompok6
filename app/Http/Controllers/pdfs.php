<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\kategoriModel;
use App\Models\adminModel;
use PDF;


class pdfs extends Controller
{
    public function cetak_pdf()
    {
        $adminModel=adminModel::
        join('menu','menu.id','=','food.menu_id')->
        select('food.*','menu.name AS kategori')->
        get();
    	$pdf = PDF::loadview('admin.download-pdf',['adminModel'=>$adminModel]);
    	return $pdf->download('daftar-menu.pdf');
    }

}
