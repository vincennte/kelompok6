<?php

namespace App\Http\Controllers;
use App\Models\kategoriModel;

use Illuminate\Http\Request;

class kategoriController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kategoriModel=kategoriModel::get();
        return view ('kategori.index',['kategoriModel'=>$kategoriModel]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kategoriModel=kategoriModel::get(); 
        return view ('kategori.add-category',['kategori'=>$kategoriModel]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $kategoriModel = new kategoriModel;
        $kategoriModel->name = $request->name; 
        $kategoriModel->save();
        return redirect('/kategori');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kategoriModel=kategoriModel::find($id); 
        return view ('kategori.edit-category',['kategori'=>$kategoriModel]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $kategoriModel = kategoriModel::find($id);
        $kategoriModel->name = $request->name; 
        $kategoriModel->save();
        return redirect('/kategori');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $kategoriModel = kategoriModel::find($id);
        $kategoriModel->delete();
        return redirect('/kategori');
    }
}
