<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\kategoriModel;
use App\Models\adminModel;

class adminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $adminModel=adminModel::
        join('menu','menu.id','=','food.menu_id')->
        select('food.*','menu.name AS kategori')->
        get();
        return view ('admin.data-table',['adminModel'=>$adminModel]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kategoriModel=kategoriModel::get(); 
        return view ('admin.add-food',['kategori'=>$kategoriModel]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $filename= time().'.'.$request->thumbnail->extension();
        $request->thumbnail->move(public_path('uploads'),$filename);

        $adminModel = new adminModel;
        $adminModel->name = $request->name;
        $adminModel->price = $request->price;
        $adminModel->description = $request->description;
        $adminModel->menu_id = $request->menu_id;
        $adminModel->thumbnail = $filename; 
        $adminModel->save();
        return redirect('/admin');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $adminModel = adminModel::find($id);
        return view('admin.detail-food',["adminModel" => $adminModel]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $adminModel = adminModel::find($id);
        $kategoriModel=kategoriModel::get(); 
        return view('admin/edit-food',["adminModel" => $adminModel],["kategoriModel" => $kategoriModel]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $filename= time().'.'.$request->thumbnail->extension('.png');
        $request->thumbnail->move(public_path('uploads'),$filename);

        $adminModel = adminModel::find($id);
        $adminModel->name = $request->name;
        $adminModel->price = $request->price;
        $adminModel->description = $request->description;
        $adminModel->thumbnail = $filename;
        $adminModel->menu_id = $request->menu_id;
        $adminModel->save();
        return redirect('/admin');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $adminModel = adminModel::find($id);
        $adminModel->delete();
        return redirect('/admin');
    }
}
