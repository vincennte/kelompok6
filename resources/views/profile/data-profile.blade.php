@extends('admin.master')
@section('title')
Data Tabel
@endsection
@section('subtitle')
Data Tabel
@endsection
@section('content')
<a href="profile/create" class="btn btn-primary">Tambah Makanan</a>
<table class="table">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Name</th>
        <th scope="col">Phone</th>
        <th scope="col">Address</th>
        <th scope="col">Detail</th>
        <th scope="col">Aksi</th>
        
      </tr>
    </thead>
    <tbody>
        @foreach ($profileModel as $profile=>$b)
        <tr>
            <th scope="row">{{$profile+1}}</th>
            <td>{{$b->name}}</td>
            <td>{{$b->phone}}</td>
            <td>{{Str::limit($b->address,50)}}</td>
            <td>{{$b->user_id}}</td>
            <td><a href="profile/{{$b->id}}" class="btn btn-info">Detail Data</a></td>
            <td>
                <form method="post" action="/profile/{{$b->id}}">
                    @csrf
                    @method('delete')
                <a href="profile/{{$b->id}}/edit" class="btn btn-warning">Edit</a>
                <button type="submit" class="btn btn-danger">Hapus</a>
                </form>
            </td>
          </tr>    
        @endforeach
    </tbody>
  </table>
@endsection