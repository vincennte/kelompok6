<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\kategoriModel;
use App\Models\adminModel;
use App\Models\Profile;
use App\Models\Review;
use DB;

class userController extends Controller
{
    public function index(){
        $adminModel = adminModel::
        join('menu','menu.id','=','food.menu_id')->
        select('food.*')->
        where('menu.name','Drink')->
        get();

        $adminModel2 = adminModel::
        join('menu','menu.id','=','food.menu_id')->
        select('food.*')->
        where('menu.name','Main Course')->
        get();

        return view('user.master',
        ["adminModel" => $adminModel],
        ["adminModel2" => $adminModel2]);
    }
    public function review($id){
        $cek = adminModel::find($id);
        return view('user.review',["cek" => $cek]);
    }
    public function rating(Request $request){
        DB::table('review')->insert([
            'user_id' => $request['user_id'],
            'food_id' => $request['food_id'],
            'comment' => $request['comment'],
            'rating' => $request['rating']
        ]);
        return redirect('/user');
    }
}
